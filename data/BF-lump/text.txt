############################################################################
#スクリプト
############################################################################
::initialize

@mapEv_act start

@setface Goblin　Girl tatie/ブランク L
@setface Lamp　Genie tatie/ブランク L

@mes 地の文1
This... this isn't what I meant to happen.
Since she told me that she would give me anything, I just asked
for all the things I wanted...

@se sound/eat1.mp3
@common_ev 500281
@picture BF-lump\cg1.jpg 0 480 360 10

@toki-puyo 40300 40350 2
@wait 70

@mes Goblin　Girl
No, don't! Stop!

@mes Lamp　Genie
As you wish, I will grant you the greatest pleasure in the 
world, you greedy child.


@mes 地の文1
While making a delivery, I came upon a beautiful lamp.
I thought about taking it home as a souvenir at first, but when
I approached it, a woman popped out.


@mes 地の文1
She asked me to tell her what I desired...


@mes 地の文1
So I told her I wanted food for me and my friends' portions 
too...


@mes 地の文1
Then some new clothes...


@mes 地の文1
A giant house to live in...


@mes 地の文1
A man to be my personal sex slave...


@mes 地の文1
And this, and that...


@wait 70
@mes 地の文1
Each item I requested magically appeared out of her purple 
smoke, one by one, but before I realized it, I was surrounded 
by the smoke myself.


#ピクチャぷよ
@toki-puyo 40300 40350 0

@mes Goblin　Girl
But you said you would give me anything!


@common_ev 500281
@picture BF-lump\cg2.jpg 0 480 360 10
#ウェイト
@wait 70

@mes Lamp　Genie
All of your greedy wishes were for your own personal enjoyment.
Therefore, I will grant you the greatest pleasure in the
world...
Which is being eaten and digested by me...

@common_ev 500281
@picture BF-lump\cg3.jpg 0 480 360 10

@toki-puyo 40300 40350 0
@se 10

@wait 70
@mes Goblin　Girl
Stop, cut it out! Let me go!


@mes 地の文1
I resist with everything I've got. Just because I'm small
doesn't mean I'm not strong enough to fight back!
I desperately grab on to the genie's upper jaw.

@toki-puyo 40300 40350 0
@se 10
@mes Goblin　Girl
Like hell I'm going to let you eat me...

@mes Lamp　Genie
Fufu, that's quite a lot of resistance...


@mes 地の文1
I only need to get free from these jaws...
If I can manage that, no one's fast enough to catch me!
I gather my strength and push back on the mouth trying to close
over me.

@mes Lamp　Genie
In that case... how about this?

@toki-puyo 40300 40350 1

@se sound/sui.mp3

@common_ev 500281
@picture BF-lump\cg4.jpg 0 480 360 10
@wait 70
@mes 地の文1
*Slurrrrrp*


@mes 地の文1
My waist is suddenly pulled into the back of her throat,
grabbed from a different direction than before.
My face freezes with the fear of not being able to escape.

@mes Goblin　Girl
Huh...? Ah... stop... what are you...!?

@mes Lamp　Genie
Fufu, if I've got you swallowed up to the waist, it should be 
fairly easy to gulp down the rest of you by wriggling my
throat, right?

@toki-puyo 40300 40350 -1

@mes Goblin　Girl
No! Stop... Help me...


@mes 地の文1
It's no use. Even though I desperately tried to hold on, she
was just playing with me all along.
It was fun for her, watching me struggle trying not to be
eaten.

@mes Goblin　Girl
Ah... I, I'm sorry...

@se sound/sui.mp3

@toki-puyo 40300 40350 1
@mes 地の文1
*Slurrrrrp*


@mes 地の文1
Once again I'm sucked deeper down her throat, and there's 
nothing in my power I can do to stop it.
I'm being eaten. I'm going to be eaten by her.

@common_ev 500281
@picture BF-lump\cg5.jpg 0 480 360 10
@wait 70
@mes Lamp　Genie
Fufu. Have you given up resisting me now?
Hoora... Shall I swallow you whole?

@toki-puyo 40300 40350 -1
@mes Goblin　Girl
No, don't... Don't eat me... Let me go...


@mes 地の文1
All I can do now is beg for forgiveness with tears streaming
down my cheeks.

@mes Lamp　Genie
Fufu, not going to happen. Just like all the others who have
drowned in lust, I'll do you the honor of devouring you.

@toki-puyo 40300 40350 2
@se sound/sui.mp3

@mes 地の文1
My body is drawn deeper down her gullet.

@common_ev 500281
@picture BF-lump\cg4.jpg 0 480 360 10
@wait 70
@mes Goblin　Girl
St... stop!

@mes Lamp　Genie
Well then, I'll swallow you in one gulp. Goodbye.

@common_ev 500281
@picture BF-lump\cg6.jpg 0 480 360 10

@toki-puyo 40300 40350 2
@se sound/eat2.mp3

@mes 地の文1
*Gulp*


@mes 地の文1
Without time for a final scream, her mouth closes over me.
Unable to resist, I'm plunged down the back of the snake's
throat.

@common_ev 500281
@picture BF-lump\cg7.jpg 0 480 360 10
@wait 70
@mes Lamp　Genie
Fufu, thanks for the meal...
Now, as you wished, please enjoy the greatest pleasure in the
world.

#ピクチャ消去
@picture DEL 0 60

#アニメ変数
@val_set 2500000 int 1
#全音停止
@se -1
@bgs -1 100 100 120
@bgs sound/roop.mp3

@mes 地の文1
*Splash* *Swish*

@common_ev 500281
@picture BF-lump\cgb1.jpg 0 480 360 10
@wait 70
@mes Goblin　Girl
Oooh... ahh... I got eaten...


@mes 地の文1
The inside of the snake's body is tightly packed with flesh,
making it very difficult to move my body at all.
The fleshy walls are so wet and slimy that I can't get a
handhold to stop myself from sliding deeper in.

@mes Goblin　Girl
Guh, I've got to get out of here somehow...

@se 10
@toki-puyo 40300 40350 1

@mes 地の文1
I attempt to escape by twisting my body side to side, but as if
mocking my efforts, the walls begin to secrete more digestive 
fluids, drop by drop.

@bgs sound/roop.mp3
@common_ev 500281
@picture BF-lump\cgb2.jpg 0 480 360 10

@se 10
@toki-puyo 40300 40350 1
@wait 70
@mes Goblin　Girl
Hiii... Please no... Stop... I'm melting, I'm really melting!


@mes 地の文1
In a few short moments, my clothes are completely melted away.
The walls of flesh press even tighter against me, making it
harder and harder to move.


@mes 地の文1
At this rate, I'm going to dissolve just like my clothes...
Imagining myself being absorbed into the genie, I feel my level
of fear gradually increase.

@mes Goblin　Girl
I'm sorry... I'm so sorry...


@mes 地の文1
I keep apologizing for some reason, but it's entirely too late
for that.
While I do so, her stomach continues to rapidly fill with
digestive juices.

@common_ev 500281
@picture BF-lump\cgb3.jpg 0 480 360 10
@wait 70
@mes Goblin　Girl
No, don't... Please don't melt me... I don't wanna die!

@mes 地の文1
The fleshy walls continue to spurt out digestive fluid 
nevertheless, and I realize that I'm now completely covered in
it.

@se 12
@common_ev 500281
@picture BF-lump\cgb4.jpg 0 480 360 10
@wait 70
@mes Goblin　Girl
Ah... faa... What is this...


@mes 地の文1
With my clothes melted, the digestive juices begin to dissolve
my skin directly.
However, rather than fear, it produces a strangely pleasant
sensation within me.

@mes Goblin　Girl
Aah... Feels... so good...♪

#アニメ変数
@val_set 2500000 int 2

@mes 地の文1
Amazing. As my body is slowly melted into slime, I'm 
unexpectedly overwhelmed by pleasure strong enough to make me 
want to orgasm.

@mes Goblin　Girl
Ah... ah, ah! ♪ So... good.♪
More... make me more slimy...


@mes 地の文1
The pleasant feelings are so intense that I completely forget 
about being afraid.
The ecstasy of being eaten attacks my body from the top of my 
head to the tips of my toes.
@se 12
@common_ev 500281
@picture BF-lump\cgb4a.jpg 0 480 360 10
@wait 70
@mes Goblin　Girl
Ahhh... ah, ah♪ Ah...♪ Coming...♪
Coming againnnnn... ♪


@mes 地の文1
The pleasure hits me so hard that a gusher of urine jets out 
between my legs.
While I pee, the fleshy walls press tighter against my body and
begin to digest me even faster.


@mes 地の文1
It's unbelievable how amazing that feels.
No, it was already amazing a long time ago.

@mes Goblin　Girl
Ah♪ Ahii, nnngh! Mo- more...
Melt me even more... nnnnhaa!


@mes 地の文1
My body is being turned to slime.
This really is the greatest pleasure in the world.
As my body melts, I finally comprehend the meaning of those 
words.



@val_set 2500000 int 0

@toki-ef 9 0
@picture BF-lump\cgb5.jpg 0 480 360 10
@wait 120
@mes Goblin　Girl
Ah... nngh... ah....

@se 12
@mes 地の文1
Filled with digestive juices, my body twitches and shakes while
I happily melt away.

@se sound/eat05.mp3

@mes 地の文1
*Gulp*


@mes 地の文1
With my body mostly gone, the snake's figure gradually returns
to its original thickness.

@mes Goblin　Girl
......

@common_ev 500281
@picture BF-lump\cgb6.jpg 0 480 360 10
@se sound/eat05.mp3


@mes 地の文1
*Gulp*

@wait 70

@mes 地の文1
Thus, the former goblin girl is steadily reduced to nothing by
the digestive juices.
In the end, she vanishes into a whirlpool of sweet ecstasy.

#ピクチャ消去
@picture DEL 0 60

#全音停止
@se -1
@bgs -1 100 100 120

@wait 70
@mes 地の文1
Today, another greedy traveler fell victim to the lamp genie.
However, perhaps dying while experiencing the greatest pleasure
in the world was exactly what she wanted all along.

@mes 地の文1
.........

#メッセ終了
@mes_RESET