############################################################################
#スクリプト
############################################################################
::initialize

@mapEv_act start

@setface Cecil tatie/ブランク L
@setface Chief tatie/ブランク L
@setface Succubus tatie/ブランク L

@mes 地の文1
My name is Cecil, and I'm a knight from North Lant.

@mes 地の文1
I travel around the world putting a stop to evil monsters in
hopes of one day running into the hero who will be our
salvation.

@mes 地の文1
During my adventure, I came across this village...
Fortunately, the village chief was kind enough to take me in.

@mes Chief
Oh my, oh my! A royal knight?
To think you've come to our humble village from so far away.
Please accept our hospitality for the night...

@mes Cecil
Thank you very much.
I'll gladly accept your offer to stay here tonight.

@mes Chief
Uhu... I'm honored.
I can't offer you much, but please enjoy a meal with me first...

@mes Cecil
Gladly! Thank you for the food!

@mes 地の文1
Soon after stuffing my face with the dishes she served, I began
to feel tired and woozy...

@mes Chief
Uhuhu... I can't believe you were caught so easily.
Hmm, should someone invite Witchy over?

@mes 地の文1
......

@mes Chief
Oh well. Her body looks so delicious.
Uhu, now it's my turn to eat...

@se sound/eat3.mp3
#ウェイト
@wait 70

@mes 地の文1
*Gulp*

@mes 地の文1
In the spacious room where Cecil and the village chief were
until just a moment ago...

@common_ev 500281
@picture LU-guest3\s01.jpg 0 480 360 10

@wait 70
@mes Succubus
Hehe, like taking candy from a baby.

@mes 地の文1
...all that remains is a succubus, who was obviously disguised
as the village chief.

@common_ev 500281
@toki-ef 4 0 10
@bgs sound/heart.mp3

@mes 地の文1
*Thump* *Thump*

@mes Cecil
What's going on... It's so warm... 
And soft... And comfortable...

@mes Cecil
Did I climb into the bath or something...?
Where in the world am I...

@mes 地の文1
I awaken in a daze... Let's see... If I recall...

@toki-ef 4 -1 10
@bgs sound/inner1.mp3

@common_ev 500281
@picture LU-guest3\s02.jpg 0 480 360 10
#ピクチャぷよ
@toki-puyo 40300 40350 0
@wait 70
@mes Cecil
!!!!?

#アニメ変数
@val_set 2500000 int 2

@mes Cecil
What? Where am I...?
How did I get here!?
Anyone! Is anyone there!?

@mes 地の文1
All I can see are walls of flesh surrounding me, dripping with
some kind of liquid.
I don't want to believe it, but this looks just like...

@mes Succubus
Oh, did you wake up?

@mes 地の文1
I hear a vaguely familiar voice echoing from somewhere.
Actually, it sounds like the voice is coming from the walls
themselves.

@mes Cecil
That voice... The village chief!?
What did you do to me!?
This looks like...!?

@mes Succubus
Oh dear, you still don't realize what happened?
Huhu... See around you? That's the inside of my stomach...\i[001]

@mes Cecil
Your stomach!? How is that possible!?

@mes 地の文1
I calm down my panicked mind and take stock of the 
surroundings.
However, the more I comprehend, the more I begin to realize 
that I'm already in a hopeless situation.

@mes Succubus
Normally we corrupt women into succubi, but a royal knight 
would kind of stick out in such a small village, don't you 
agree?

@mes Succubus
So... huhu... I ate you.\i[001]
I used magic to shrink you down to size, and then... *Gulp*\i[001]

@mes Cecil
What...!? You ate...!?

@mes 地の文1
Ah, so the village chief was a succubus in disguise.
She drugged me at dinner and then swallowed me while I was 
unconscious.

@mes Cecil
Ge-get me out of here! Let me out!

@mes Succubus
Uhuhu... Don't be afraid.
This won't hurt one bit, okay?

@mes Succubus
It's not like normal digestion. 
I'll gently melt your flesh, mind, and soul into syrup... and 
then absorb all of it.

@mes Cecil
No way! Ah, aaah!

@val_set 2500000 int 3
@se sound/nukarumu.mp3
@bgs sound/roop1.mp3
@common_ev 500281
@picture LU-guest3\s03.jpg 0 480 360 10
@wait 70
@mes 地の文1
A feeling of hopelessness sets in as her stomach begins pulsing
around me.
Digestive fluids pour out, immersing my body in their slimy
viscosity.

@mes Cecil
No! I'm... melting...? Huh...?

@mes 地の文1
I'm being digested alive. 
Naturally, my body should be on fire and in considerable pain.

@mes 地の文1
However, as soon as I'm covered in digestive fluids, my body is
flooded with a strange sense of comfort and pleasure.

@mes Cecil
What the... Why... does this feel so good...

@mes Succubus
Uhu... I'm glad to hear you're enjoying yourself in there.
A succubus's bodily fluids are a powerful aphrodisiac to 
humans.
Please relax and make yourself comfortable...

@mes Cecil
No! Stop... ngh... I don't want... aah...\i[001]

@mes 地の文1
My thoughts begin to wander.
The pleasant feelings are nearly strong enough to make me 
orgasm and fill me with a frightening sense of security.

@mes 地の文1
Even though my sensitive body is drowning in pleasure, I'm 
assailed by a comforting drowsiness.

@mes 地の文1
If I fall asleep like this, I'll reach climax over and over 
while unconscious...
And while I do, I'll melt away... and probably never wake up 
again.

@mes Succubus
Uhu... You can't possibly escape now...\i[001]
Doesn't it feel good to slosh around and have your whole body 
painted in digestive fluids?

@mes Succubus
Come on, abandon your mind and body to the pleasure.
Don't you want to become one with me?

@mes 地の文1
The succubus's seductive voice reverberates inside my skull.
Her words poison my mind, causing the will to resist to drain
from my body.

@se sound/mizu05.mp3
@common_ev 500281
@picture LU-guest3\s04.jpg 0 480 360 10
@wait 70

@mes Cecil
Not... good... Mustn't... give up...\i[001]

@mes Succubus
Hehe, you're really delicious, by the way.
Having strong-willed prey slowly melt in pleasure inside me, 
unable to do anything, is an amazingly wonderful sensation...\i[001]

@mes Cecil
Ah... aaaaah...

@mes 地の文1
My body won't move any longer. 
No, even if it could move, it wouldn't make any difference. 

@mes 地の文1
I want to give myself over to the succubus and forget 
everything else.
That'd be great...


@mes Succubus
Come on, keep melting.

@mes Cecil
Ah... Nnngh...\i[001]


@val_set 2500000 int 2
@se sound/nukarumu.mp3
@common_ev 500281
@picture LU-guest3\s04.jpg 0 480 360 10
@wait 70

@mes 地の文1
Just as soon as that seed was planted in my mind, it began to 
sprout, sending my body into orgasmic convulsions.
It wasn't only my mind that was desiring this pleasure...

@mes 地の文1
I accept it, and from the bottom of my heart, I surrender to 
heaven.

@mes Cecil
Good... so good... Melt me more...\i[001]

@mes Succubus
Uhuhu... No problem.
Now then, drown in the pleasure of your whole body melting 
away...\i[001]

@se sound/nukarumu.mp3
@common_ev 500281
@picture LU-guest3\s04.jpg 0 480 360 10
@wait 70

@mes 地の文1
The succubus gives her belly a loving rub, and digestive juices
begin flooding into her stomach.

@mes Cecil
Aaaah, hurry... Hurry up and melt me...
Ngh... Nnnnngh!\i[001]

@common_ev 500281

@val_set 2500000 int 1

@se sound/mizu09.mp3
@picture LU-guest3\s05.jpg 0 480 360 10
@wait 70
@mes 地の文1
*Rumble*
*Gurgle*


@val_set 2500000 int 0

@wait 70
@common_ev 500281
@bgs -1 100 100 120
@se sound/eat2.mp3
@picture LU-guest3\s06.jpg 0 480 360 10
@wait 70

@mes 地の文1
*Gulp*

@se 15

@mes 地の文1
Once my body is completely enveloped in digestive fluids, I 
lose consciousness mid-orgasm...
Digested by the succubus, my existence itself is absorbed.

@mes Succubus
Huhu... Thanks for the treat.\i[001]
@common_ev 500281
@toki-ef 4 0 10

@mes 地の文1
Thus, the wandering knight's life came to an end:
eaten, toyed with, and digested inside the stomach of a 
succubus before her duty was fulfilled.

@wait 80

#ピクチャ消去
@picture DEL 0 60

#全音停止
@se -1
@bgs -1 100 100 120

@toki-ef 4 0 -1

@mes 地の文1
.........
@wait 80


#メッセ終了
@mes_RESET
