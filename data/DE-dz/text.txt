############################################################################
#スクリプト
############################################################################
::initialize

@mapEv_act start

@setface Zombie　Dragon　Girl tatie/ブランク L
@setface Chrome tatie/ブランク L

@mes Zombie　Dragon　Girl
Kukuku... What's wrong?
Can't do anything about it...?
@mes Chrome
Uh, uaa...there are zombies like you? I need to summon 
Frederica...!

@mes 地の文1
I fell into the pit, and got lost...
I cursed the absence of my luck.
@mes 地の文1
However, now when I aproached Zombie Dragon Girl, that doesn't 
matter.
@mes Zombie　Dragon　Girl
Kukuku... I have caught a human male a while ago...and now I 
have a succubus...
@mes Chrome
Eh... a human male.....could it be---


@mes 地の文1
Luka---
@se sound/swing.mp3
@mes 地の文1
Before I could react, a human-shaped organ jumped out if 
dragon's mouth and caught me like a frog's tongue!
@se sound/byoro01_a.mp3
@toki-shake 40300 40350 05 42
@common_ev 500281
@picture DE-dz\dz02.jpg 0 480 360 10
#アニメ変数
@val_set 2500000 int 1
#ピクチャぷよ
@toki-puyo 40300 40350 2
@wait 70

@mes 地の文1
And in the next moment, I was drawn into the mouth of Zombie 
Dragon Girl.
@mes Chrome
Hii, Hiii......!
Le-let me go...!!
@se sound/attack00.mp3

@val_set 2500000 int 2
@mes Zombie　Dragon　Girl
Kukuku...Why should I release prey that is already in my mouth?


@mes 地の文1
Zombie Dragon Girl grabbed my head, I'm being gradually drawn 
into the back of her mouth.

@common_ev 500281
@picture DE-dz\dz01.jpg 0 480 360 10

@wait 70
@mes Chrome
Guh......ha.....
Stop......it......
@se sound/damage.mp3
@toki-shake 40300 40350 05 42

@mes 地の文1
Despite my desperate struggle, my opponent is a huge dragon.
Moreover, her zombie brain is protected from hacking.
I can't take control over her. 
@mes Chrome
Ah.....guh....a......
@mes Zombie　Dragon　Girl
Kuku...Is that the best you got...?
You are just like a child.....


@mes 地の文1
Now matter how much I resist, it's impossible to escape from 
her mouth.
Even with the power of two gosts it's still pointless...

@se sound/jururi.mp3
@common_ev 500281
@picture DE-dz\dz03.jpg 0 480 360 10

@wait 70
@mes Zombie　Dragon　Girl
Here...let me taste you...

@mes 地の文1
In other words, I had no way to escape.
@se sound/gutyu0.mp3
@mes Chrome
Uh.....uh...uuh.....

@mes 地の文1
The tongue of Zombie dragon girl stretched out, and started to 
lick my face making me shiver.
@mes 地の文1
My face is in fear and discomfort...
@mes Chrome
I-I'm not tasty...!

@mes 地の文1
Except begging for my life, there is nothing more I can do.
@mes Chrome
I rarely taking a bath...
I'm touching corpses with those hands...
If you'll eat me, you will surely get a stomach ache!


@mes 地の文1
My crying voice is the only sound echoing in this quiet place.
I'm desperately scream while Zombie Dragon Girl tasting me 
silently.
@mes Chrome
So, so you...
@mes Zombie　Dragon　Girl
Kuku...don't worry about it...as a zombie, I don't have a picky
taste...
@se sound/jururi.mp3
@common_ev 500281
@picture DE-dz\dz04.jpg 0 480 360 10

@wait 70
@mes Zombie　Dragon　Girl
And I can assure you...that even your bones will melt away in 
my stomach...

@mes Chrome
Hii...!


@mes 地の文1
Now, with her long tounge, Zombie Dragon Girl licking my neck 
too.

@se sound/kucha03.mp3
@common_ev 500281
@picture DE-dz\dz05.jpg 0 480 360 10

@wait 70
@mes Zombie　Dragon　Girl
*Lick*...*Lap*...kuku...it's soft and very nutritious meat...
and it has a plenty of magical power too...
Kukuku...I'm sure you will make a great meal...

@mes Chrome
Hii, hiiii...!

@mes 地の文1
She keeps licking my face to savor my taste.
@common_ev 500281
@picture DE-dz\dz06.jpg 0 480 360 10

@wait 70
@mes 地の文1
And when she finally enjoyed it, she putted her long tongue 
back in her mouth.
@mes Zombie　Dragon　Girl
Alright...are you ready...?
I am going to swallow that body of yours...
And while it will be in my stomach, it will be digested into 
mush...

@mes Chrome
Hii, pllllllease!
I'll do anything...
Please...I don't want to be eaten...!


@mes 地の文1
However, excited Zombie dragon girl couldn't even hear such a 
things like begs for my life any longer.
@se sound/animal02.mp3
@common_ev 500281
@picture DE-dz\dz07.jpg 0 480 360 10

@wait 70
@mes Zombie　Dragon　Girl
Look at that...my upper jaw is gradually closing...
Hey...you better move your neck and torso, or you would like to
be sliced in two...?

@mes Chrome
Help...
@mes Zombie　Dragon　Girl
Kuku, I can't hold myself anymore.
Ok...let me swallow you whole...!
@common_ev 500281
@picture DE-dz\dz08.jpg 0 480 360 10

@wait 70

@mes Chrome
Me.....!
@toki-shake 40300 40350 05 42
@se sound/bosu25_a.mp3
@common_ev 500281
@picture DE-dz\dz09.jpg 0 480 360 10
@toki-puyo 40300 40350 4

@val_set 2500000 int 0
@wait 70

@mes 地の文1
*CHOMP*
@mes 地の文1
And with loud sound, the huge mouth closed tightly.

@mes 地の文1
My bag that was partially outside, was chomped like a piece of 
bread, and it was the last thing I saw.
@se sound/gokkunn6.mp3
@toki-ef 4 0 10
@mes 地の文1
And the moment after, I was dragged to the throat, and 
swallowed with a loud sound, and the outside was filled with 
complete silence...

@toki-ef 4 -1 10
@val_set 2500000 int 2
@bgs sound/kutyu2.mp3
@common_ev 500281
@picture DE-dz\dzb01.jpg 0 480 360 10

@wait 70

@mes Chrome
Uhah...ah......aah......


@mes 地の文1
Zombie dragon girl's stomach filled with corpse's odor, and it 
is full of sticky digestive juices and warmth.
@mes Zombie　Dragon　Girl
Welcome...to my stomach...


@mes 地の文1
With my body being hugged, and swallowed in the stomach that is
painted with digestibe juices, my consciousness became blurry.
@common_ev 500281
@picture DE-dz\dzb02.jpg 0 480 360 10

@wait 70

@mes Chrome
No no no... stop it...
@se sound/damage.mp3
@toki-shake 40300 40350 05 42

@mes 地の文1
I tryed to restart my weak resistance.
However, while being sandwiched by those resilient big breasts,
and with my arms binded, I couldn't struggle any longer.
@mes Zombie　Dragon　Girl
Kukuku...you are really cute...you look just like a doll...

@se sound/attack00.mp3
@mes 地の文1
Zombie Dragon Girl is stroking my head, she is sandwiching it 
in between her big breasts.
@common_ev 500281
@picture DE-dz\dzb03.jpg 0 480 360 10
@toki-puyo 40300 40350 2

@wait 70

@mes Chrome
Uh...fuaah....aaah.....


@mes 地の文1
Euphoric effect of digestive juices, smell of her stomach, and 
her caress to me quickly taking control over my mind and body.

@mes Chrome
It...uh...ah.....feels...good.....
@mes Zombie　Dragon　Girl
Kukuku...succubus is a succubus...you are always honest with 
pleasure...
@se sound/kutyu3.mp3
@common_ev 500281
@picture DE-dz\dzb04.jpg 0 480 360 10

@wait 70

@mes Chrome
Ah.....nmm...hiaaaaah.....\i[001]
@mes Zombie　Dragon　Girl
Kukukuku...well then...enjoy this pleasure...to your last 
moments...
@bgs sound/kutyu4.mp3
@se sound/nukarumu.mp3
@toki-shake 40300 40350 05 42

@mes Chrome
Hiyaah...nn.....!
Nyah....! Ah.....haaaaaaaah...!


@mes 地の文1
While my body is melting in digestive juices, 
Zombie Dragon Girl stroking it with her big hands.
She gently rubbing her hand covered in digestive juices into me
to speed up the digestive process.


@mes 地の文1
As that liquid soaked into my sensitive area, I started to 
tremble in ecstasy.
@mes Zombie　Dragon　Girl
How is it...? How do you feel...?
As your body melts from the outside...it also partially melts 
from the inside...

@toki-shake 40300 40350 05 42

@mes Chrome
Nnn.....aah.....ha!
Stop.....aah......\i[001]
@mes Zombie　Dragon　Girl
Kukuku...okay...then I'll be even more gentle...
@bgs sound/feratyupa2.mp3
@se sound/jururi.mp3
@common_ev 500281
@picture DE-dz\dzb05.jpg 0 480 360 10

@wait 70

@mes Chrome
Hii...aaaaahh!


@mes 地の文1
Zombie Dragon Girl's lukewarm tounge has extended, and started 
to gently lick my ear, horn, neck and so on.

@mes Chrome
Nn....aah.....
Not like this.....hiaaa...aaah...!
It...feels...too.....good...!\i[001]


@mes 地の文1
While my body is covered with saliva and digestive juices, I 
tremble in agonizing pleasure.
@mes 地の文1
I also forgot the fact that I am being digested...
@se sound/kutyu3.mp3
@toki-shake 40300 40350 05 42

@mes Chrome
Hiaa.....I'm...coming.....
Stop.....ah...!


@mes 地の文1
Succubus who is forced to come by other creature.
Even though I understand how shameful it is for our race, it 
doesn't matters for me, because of digestion.
I lost my mind after accepting this pleasure.
@mes Zombie　Dragon　Girl
Kuku...then...just let it out...let out...everything that's 
inside...
Everything of you will be digested...anyway...
@se sound/lick_s5.mp3
@toki-shake 40300 40350 05 42

@mes Chrome
Hiaaa...ah, aahhhh......\i[001]


@mes 地の文1
Embarrassment, regret, pleasure, everything got messed up, and 
mixed in me.

@mes Chrome
I'm.....co...ming.....nnmph!
@bgs sound/gutyu.mp3
@common_ev 500281
@picture DE-dz\dzb06.jpg 0 480 360 10

@toki-ef 2 0
@wait 70


@mes 地の文1
*Splash* My body bounces in the arms of Zombie dragon girl.
At the same time, amount of love juices blew out of my crotch.
@common_ev 500281
@picture DE-dz\dzb07.jpg 0 480 360 10

@val_set 2500000 int 1
@wait 70

@mes Chrome
Ah.....auh......ah...uh...

@mes 地の文1
While my body is trembling and wriggling, my eyes are focused 
on nothing.
@mes 地の文1
I can't think of anything anymore...

@mes Chrome
Awah.....I came...\i[001]


@mes 地の文1
I am fully at her mercy, as my heart and body succumbed, I 
stopped thinking.
@mes Zombie　Dragon　Girl
Kukuku...so cute....you came unintentionally...
@se sound/crash18_c.mp3
@toki-shake 40300 40350 05 42
@common_ev 500281
@picture DE-dz\dzb08.jpg 0 480 360 10

@wait 70

@mes Chrome
Wah...guh.....


@mes 地の文1
Strengthening her grip, Zombie Dragon Girl embraced me even 
tighter.
As her internal organ begun to constrict, it became hard to 
breathe and my consciousness started to fade...
@mes Zombie　Dragon　Girl
Well... I'll digest you while hugging you like this...
Kukuku...Everything will be over for you soon and you will feel 
better...
@se sound/mizu09.mp3
@common_ev 500281
@picture DE-dz\dzb09.jpg 0 480 360 10
@bgs sound/roop1.mp3

@wait 70

@mes 地の文1
The stomach walls are constricting with rumbling noices.
Gently and slowly, it's wrapping the prey who stopped her 
resistance...
@mes Zombie　Dragon　Girl
Now... go to sleep, okay?...
Forget everything...while relaxing in between my chest...
@se sound/mizu09.mp3
@common_ev 500281
@picture DE-dz\dzb10.jpg 0 480 360 10


@wait 70
@mes Chrome
Fua....a...♪
@mes Zombie　Dragon　Girl
Kukuku...

@mes Chrome
Ah...uh...awa.....♪


@mes 地の文1
With blind eyes, I can see the slowly approaching stomach 
lining.
My body already covered in digestive juices body begining to 
collapse, the absorption has begun.
@mes 地の文1
And since my body is completely wrapped in the stomach walls...
then withing a few minutes, it will leave no trace, it will 
melt into nothing.

@mes Chrome
Nn....ha.....♪


@mes 地の文1
But there is no way to avoid it and no way to escape.
I completely accepted my fate.
@common_ev 500281
@picture DE-dz\dzb11.jpg 0 480 360 10

@se 12
@wait 70
@mes Chrome
Haa...haa...I...am...melting...ha...\i[001]
@se sound/lick_s3.mp3
@common_ev 500281
@picture DE-dz\dzb12.jpg 0 480 360 10



@wait 70
@mes 地の文1
*Smack*
@se sound/nukarumu.mp3
@toki-shake 40300 40350 05 42
@toki-puyo 40300 40350 2
@mes 地の文1
*Squish*
@se sound/lick_s5.mp3
@toki-shake 40300 40350 05 42
@toki-puyo 40300 40350 2
@mes 地の文1
*Chew*
@se sound/lick_s4.mp3
@toki-shake 40300 40350 05 42
@toki-puyo 40300 40350 2
@mes 地の文1
*Slosh*
@se sound/gokun5.mp3
@common_ev 500281
@toki-ef 4 0 10
@val_set 2500000 int 0

@wait 70
@mes 地の文1
*Gulp*
@mes 地の文1
.........
@se sound/gokkunn6.mp3

@bgs -1 100 100 120

@wait 70

@mes Zombie　Dragon　Girl
Kukuku...Thank you for the meal.....


@mes 地の文1
And like that, greedy dragon has obtained another prey.

@mes 地の文1
Forever and ever, this supplier of death will eat and digest 
everyone without leaving even bones.
Anything ending up in her mouth, a human being, a monster, all 
will meet the same fate.

#ピクチャ消去
@picture DEL 0 60

#全音停止
@se -1
@bgs -1 100 100 120


@mes 地の文1
.........
@wait 80


#メッセ終了
@mes_RESET

