############################################################################
#スクリプト
############################################################################
::initialize

@mapEv_act start

@setface Dragon　Pup tatie/ブランク L
@setface Shirohebi tatie/ブランク L
@setface Kejouru tatie/ブランク L

@mes 地の文1
A few days after Black Alice's army conquered the village.
And while the conquest is advancing...
@mes Kejouru
Good evening, Shirohebi-sama.
@mes Shirohebi
Oh?...Good evening...
Is sacrifice ready?...
@mes 地の文1
All prisoners of war, were turned into slaves... or were 
eaten... or...

@mes Shirohebi
Kukuku...today's sacrifice seems to be delicious...

@mes Dragon　Pup
Uh, guh!...St-stop.....!
@mes Shirohebi
Don't struggle...sacrifice is supposed to stay still...
@se sound/eat.mp3

@mes Dragon　Pup
Uhaa...!? Let me go!...Ugaaaaaa......!
@se sound/eat3.mp3
@mes 地の文1
*Chomp*


@common_ev 500281
@picture DE-sh\sh01.jpg 0 480 360 10
#ピクチャぷよ
@toki-puyo 40300 40350 2
@se sound/eat2.mp3

@wait 70

@mes 地の文1
*Gulp*
@mes Shirohebi
You'd better stay in my stomach..
@mes 地の文1
This is the relationship...of a predator and prey.


#アニメ変数
@val_set 2500000 int 1
@mes Shirohebi
Ahhh.....


@mes Shirohebi
Kuku...little prey like this...is impossible to savor througly 
because of the small size....
@mes 地の文1
Satisfied, Shirohebi patting her swollen belly.

@mes 地の文1
While inside of it...
@bgs sound/inner1.mp3

@common_ev 500281
@picture DE-sh\shc01.jpg 0 480 360 10

@wait 70
@mes Dragon　Pup
Ug-ugaaa...! I'm...
I was...eaten...!?
@mes 地の文1
A small child dragon was still alive.

@mes Dragon　Pup
Ug-ugaaaa! I have to get out!


@val_set 2500000 int 3
@se sound/mizu09.mp3
@common_ev 500281
@picture DE-sh\sh02.jpg 0 480 360 10
@wait 15
@common_ev 500281
@picture DE-sh\sh03.jpg 0 480 360 10
@wait 15
@common_ev 500281
@picture DE-sh\sh02.jpg 0 480 360 10

@wait 70
@mes 地の文1
Dragon pup desperately struggles in the stomach.

@mes 地の文1
Although she is small, she is a dragon after all.
Her strength is far above any human, she is desperately trying 
to screw up that stomach.

@mes 地の文1
However...
@se sound/mizu09.mp3
@common_ev 500281
@picture DE-sh\sh04.jpg 0 480 360 10
@wait 15
@common_ev 500281
@picture DE-sh\sh05.jpg 0 480 360 10
@wait 15
@common_ev 500281
@picture DE-sh\sh04.jpg 0 480 360 10
@wait 15

@wait 70

@mes 地の文1
*Rumble*... *Rumble*...

@mes Shirohebi
That... tickles...
@mes 地の文1
Just scratching a bit inside Shirohebi will not give her any 
good.

@mes 地の文1
Dragon pup desperately scratching with her claws.
However, difference between her strength and power of the great
white snake is too high.
Resistance is completely pointless.
@se sound/lick_s4.mp3
@common_ev 500281
@picture DE-sh\sh05.jpg 0 480 360 10

@wait 70

@mes 地の文1
*Squish*

@se sound/lick_s5.mp3
@common_ev 500281
@picture DE-sh\sh04.jpg 0 480 360 10

@wait 70

@mes 地の文1
*Slosh*

@mes 地の文1
The swollen belly wriggles every time she violently moves.
But for Shirohebi, the feeling of her prey trying to escape out
of her stomach, was just a usual part of feeding.

@mes Shirohebi
Kukuku...such an energetic prey...
Squirming little dragon is such a pleasant treat.
@bgs sound/roop1.mp3
@common_ev 500281
@picture DE-sh\shb01.jpg 0 480 360 10

@wait 70

@mes Dragon　Pup
Uga...I, I have to get out somehow...! or I will be digested...!
@se sound/damage.mp3
@toki-shake 40300 40350 05 42

@mes 地の文1
Dragon pup was still resisting inside of narrow body.

@common_ev 500281
@picture DE-sh\shb02.jpg 0 480 360 10

@wait 70

@mes Dragon　Pup
Uga...uga.....aaaahh...!
@se sound/damage.mp3
@toki-shake 40300 40350 05 42

@mes 地の文1
Gathering all remaining strength of her body, she is pushing 
herself against the walls.

@mes Dragon　Pup
Uga...ugaahhhh...!
@se sound/damage.mp3
@toki-shake 40300 40350 05 42

@mes 地の文1
However―――
@se sound/nukarumu.mp3
@common_ev 500281
@picture DE-sh\shb03.jpg 0 480 360 10

@val_set 2500000 int 1
@wait 70
@mes Dragon　Pup
U..ga...ah.....
Haa...haa...it, it is completely...useless...
@mes 地の文1
No matter how hard a child dragon scratching with her claws, or
even biting with her fangs, her fleshy prison showing no signs 
of opening at all.



@mes 地の文1
On the contrary―――
@se sound/attack00.mp3
@common_ev 500281
@picture DE-sh\shb04.jpg 0 480 360 10

@wait 70

@mes 地の文1
*SQUEEZE*

@mes Dragon　Pup
Uga...!? It's, it's so.....tight.....
@mes 地の文1
The stomach began to narrow, crushing the dragon pup inside, 
and it made the walls even thicker.

@se sound/gulp_l.mp3
@mes Dragon　Pup
Wa...ahh....haah....u....gaa.....


@mes 地の文1
With the lack of oxygen because of her rampage, Dragon pup 
started to breathe harder.
While her consciousness starting to become blurry.
The digestive process proceeds quietly.

@se sound/lick_s5.mp3
@common_ev 500281
@picture DE-sh\shb05.jpg 0 480 360 10

@wait 70


@val_set 2500000 int 2
@mes Dragon　Pup
Uga.......!?
This.....can't...be.....!
@mes 地の文1
As stomach walls has started to secrete digestive juices,
the clothes she wore began to melt, exposing her small breasts.

@mes Dragon　Pup
Hia.....ah, am I...going...to be... melted...like this...?

@mes 地の文1
The moment she looked at her clothes that melted away without 
leaving a trace, her face became pale.

@mes Dragon　Pup
I-I don't...want...this.....


@se sound/nukarumu.mp3
@mes 地の文1
Dragon pup desperately begs for her life.
But her pleads reached no one, and digestive juices continues 
to fill the stomach.
@se sound/Bubble16.mp3
@common_ev 500281
@picture DE-sh\shb06.jpg 0 480 360 10

@wait 70
@mes Dragon　Pup
Ugaah.....it's.......soft...and...warm.....

@mes 地の文1
This digestion is completely painless, while enveloping body in
tender warmth, it even gives pleasant sensation.

@val_set 2500000 int 1
@mes Dragon　Pup
Uga...♪ Feeling...so good......♪

@mes 地の文1
The more of her body being enveloped in digestive juices, the 
higher pleasure.
@mes Dragon　Pup
Uga....ahh...♪
It's so warm...and slimy... and soft..... I want...more...♪
@mes 地の文1
While covering her body, the pleasure of being digested, is 
melting her heart faster than body.
@se sound/makituki.mp3
@common_ev 500281
@picture DE-sh\shb07.jpg 0 480 360 10

@wait 70

@mes 地の文1
*Constrict*

@mes 地の文1
Then, as if having mind of its own, stomach lining narrowed 
even tighter, gently hugging Dragon Pup.

@se sound/lick_s4.mp3
@mes Dragon　Pup
Ga........♪

@mes 地の文1
As her whole body was sandwiched in the stomach, she couldn't 
resist the digestive juices anymore.

@mes 地の文1
And the pleasure of being digested, makes her feel as good as 
possible.

@mes Dragon　Pup
Uga....I feel...so...comfortable.....♪
Give.....me....mo...re...♪

@mes 地の文1
As the digestive juices continued to overflow, increasing its 
concentration, Dragon pup's body is melting little by little,
and becoming part of Shirohebi's body.

@se sound/lick_s3.mp3
@mes Dragon　Pup
Ahh...nn......nn.....

@mes 地の文1
Dragon's claws that should be harder than iron, scales that can
withstand lava are getting softer, it slowly decomposing and 
melting away.
@se sound/lick_s5.mp3
@mes Dragon　Pup
Uaah...nn.....ahu.....ah.....
@mes 地の文1
Dragon pup's panting voice is getting calmer, as she began to 
enjoy the pleasure.
Enjoying the pleasure of being digested, she lost her virginiy.

@mes Dragon　Pup
Ah...haa...I'm getting horny.....
It's up to my tail..... it melts....nmm...my pussy...
It seems I'm going to... pee...♪

@se sound/water08.mp3
@common_ev 500281
@picture DE-sh\shb08.jpg 0 480 360 10

@wait 70

@mes 地の文1
Ah.....fua...aaah.....nm...♪

@mes 地の文1
While being covered in digestive juices, and trembling in 
pleasure, Dragon pup is leaking urine.


@mes 地の文1
She already completely accepted her consumption, she accepted 
her role as food to be digested, she wants herself to be 
absorbed.

@mes Dragon　Pup
Wah...haaaaah....ege.....he...♪
@se sound/mizu09.mp3
@common_ev 500281
@picture DE-sh\shb09.jpg 0 480 360 10

@wait 70
@mes 地の文1
*Rumble*

@mes 地の文1
As the walls of flesh narrowed even more, there was nothing to 
stop it from absorption of the digested body.

@mes 地の文1
This process will not be stopped, until the body of the prey 
completely disappear in the stomach of predator.
@se sound/bubble6.mp3
@common_ev 500281
@picture DE-sh\shb10.jpg 0 480 360 10

@wait 70
@mes Dragon　Pup
U...ga......♪

@mes 地の文1
While being immersed in the sea of digestive juices and being 
dissolved, Dragon pup is getting absorbed with a happy face 
expression.
@mes 地の文1
Scale, claws, tail, hair, flesh...
Everything became a simple food for Shirohebi.
@se 12
@toki-ef 4 0 10
@mes Dragon　Pup
U.........ga.........♪

@val_set 2500000 int 0
@se sound/gutyu0.mp3
@mes 地の文1
With the feeling like ascending to heaven, the body of 
Dragon Pup was absorbed.
………

@toki-ef 4 -1 10
@bgs sound/gokun5.mp3

@common_ev 500281
@picture DE-sh\sh06.jpg 0 480 360 10

@wait 70
@mes Shirohebi
Kukuku...it's about time... you must be fully digested now...

@mes 地の文1
And when the digestion was completely over, Shirohebi's abdomen 
gradually returns to the original size little by little.
At the late evening, in the dark room, one serpent slowly 
digested her prey and absorbed it.

@mes 地の文1
The melted flesh flows from the stomach to the backside of her 
tail, sounds of absorption is the only thing can be heard in 
the room.

@se sound/gulp_l.mp3
@common_ev 500281
@picture DE-sh\sh07.jpg 0 480 360 10
#ピクチャぷよ
@toki-puyo 40300 40350 1
@wait 70

@mes 地の文1
*Gulp* *Gulp*


@se sound/gokkunn6.mp3
@common_ev 500281
@picture DE-sh\sh08.jpg 0 480 360 10

@toki-puyo 40300 40350 1
@wait 70

@bgs -1
@mes 地の文1
*Gulp*



@mes Shirohebi
Mm....haa.....
Kuku...it's so pleasant...


@mes 地の文1
After digesting prey in her belly, Shirohebi felt pleasant 
relief, and blush appeared on her cheeks.


@common_ev 500281
@picture DE-sh\sh09.jpg 0 480 360 10

@wait 70
@mes Shirohebi
Now...

@mes 地の文1
Shirohebi flips the hem of kimono.
Under it, a predatory mouth showed itself.


@val_set 2500000 int 1
@mes Shirohebi
Nn...
@se sound/lick_s3.mp3
@common_ev 500281
@picture DE-sh\sh10.jpg 0 480 360 10

@toki-puyo 40300 40350 1
@wait 70
@mes 地の文1
*Squish*

@mes 地の文1
From the split of the sensitive part, a headdress that 
Dragon pup has wore was shown.
Getting out with squishing sounds, it was spitted out.

@mes Shirohebi
Fufu...that was delicious mm.....
@se sound/nukarumu.mp3
@common_ev 500281
@picture DE-sh\sh11.jpg 0 480 360 10

@toki-puyo 40300 40350 1
@wait 70
@mes 地の文1
*Squish*
@se sound/bosu18_a.mp3

@common_ev 500281
@picture DE-sh\sh12.jpg 0 480 360 10


@val_set 2500000 int 0
@toki-puyo 40300 40350 2
@wait 70

@mes 地の文1
*Thud*
Then, that covered in liquid decoration, quietly fell on the 
floor.

@mes Shirohebi
Kukuku...kukukuku.....it was a feast.....
@common_ev 500281
@picture DE-sh\sh13.jpg 0 480 360 10
@se 15
@wait 70
@mes Shirohebi
Alright...who shall I devour next?...

@mes 地の文1
In the dark room, Shirohebi's eyes started to shine and her 
tail is twitching playfully.
That facial expression means, that predator already seeking for
a new prey.
@common_ev 500281
@toki-ef 4 0 10

@mes 地の文1
After that, how many humans, how many creatures, were played 
with and eaten alive by this serpent?
There is no one alive who can answer this question.

@wait 80

#ピクチャ消去
@picture DEL 0 60

#全音停止
@se -1
@bgs -1 100 100 120

@toki-ef 4 0 -1

@mes 地の文1
.........
@wait 80


#メッセ終了
@mes_RESET